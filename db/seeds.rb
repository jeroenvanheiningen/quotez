# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'rest-client'
require 'json'
result = RestClient.get('http://quotes.stormconsultancy.co.uk/quotes.json')
return if result.code != 200

JSON.parse(result.body).each do |q|
  Quote.create(author: q['author'], quote: q['quote'])
  puts "added quote from #{q['author']}."
end

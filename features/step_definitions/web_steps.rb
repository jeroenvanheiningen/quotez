Given('I am on the home page') do
  visit root_path
end

Given('the following quotes exist:') do |table|
  # table is a Cucumber::MultilineArgument::DataTable
  table.hashes.each do |quote|
    Quote.create!(quote)
  end
end

Then('I should see {string}') do |string|
  expect(page).to have_content(string)
end

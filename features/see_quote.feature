Feature: see quotes
  In order to have fun
  As a user
  I want like to see a random quote

Background: added quotes
  Given the following quotes exist:
  | author |             quote |
  | me     | something awesome |

Scenario: Seeing a quoute
  Given I am on the home page
  Then I should see "something awesome"
  And I should see "me"

# Quotez
A random quote generator site

[![pipeline status](https://gitlab.com/jeroenvanheiningen/quotez/badges/master/pipeline.svg)](https://gitlab.com/jeroenvanheiningen/quotez/commits/master) [![coverage report](https://gitlab.com/jeroenvanheiningen/quotez/badges/master/coverage.svg)](https://gitlab.com/jeroenvanheiningen/quotez/commits/master)

* Ruby version
`2.5.1`

* Database creation
```
  rails db:migrate
```

* Database initialization
```
  rails db:seed
```

* Run quotez on bare metal

    Make sure you are on Ruby `2.5.1`
    ```
    bundle install
    rails db:create
    rails db:migrate
    rails db:seed
    ```

* Run quotez in a docker container
```
docker-compose run rails rake db:create
docker-compose run rails rake db:migrate
docker-compose run rails rake db:seed
docker-compose up
```

* How to run the test suite
```
bundle exec rspec
bunlde exec cucumber
```

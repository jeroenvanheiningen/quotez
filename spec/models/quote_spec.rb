# == Schema Information
#
# Table name: quotes
#
#  id         :integer          not null, primary key
#  author     :string
#  quote      :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Quote, type: :model do
  it 'constructor must construct' do
    q = Quote.create(author: 'me', quote: 'something amazing')
    expect(q.valid?).to be true
  end

  it 'valid must be false when no author is Given' do
    q = Quote.create(quote: 'something amazing')
    expect(q.valid?).to be false
  end

  it 'valid? must be false when not quote is given' do
    q = Quote.create(author: 'me')
    expect(q.valid?).to be false
  end
end

FROM ruby:2.5.1
# Create app directory
WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock ./
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash - &&\
    apt install nodejs -y &&\
    bundle install

COPY . .

CMD ["rails", "server", "-b", "0.0.0.0", "-p", "3000"]

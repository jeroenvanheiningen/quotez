# == Schema Information
#
# Table name: quotes
#
#  id         :integer          not null, primary key
#  author     :string
#  quote      :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Quote < ApplicationRecord
  validates :author, :quote, presence: true
  scope :random, -> { offset(rand(Quote.count)).first }
end

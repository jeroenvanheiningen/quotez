# application helper
module ApplicationHelper
  def random_background
    colors = %w[blue orange pink yellow green]
    "bg-#{colors.sample}"
  end
end
